package com.dspread.demoui.utils;

public interface TitleUpdateListener {
    void sendValue(String value);
}
